DROP DATABASE IF exists progettoecomerce;
CREATE DATABASE progettoecomerce;
use progettoecomerce;

CREATE TABLE utente(
utenteId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(50) NOT NULL UNIQUE,
pass varchar(150) NOT NULL,
tipologia VARCHAR(20) CONSTRAINT CHECK(tipologia IN ("admin","store","simple"))
);

CREATE TABLE ordine(
 ordineId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
 dataOrdine datetime DEFAULT current_timestamp,
 codiceOrdine VARCHAR(50) NOT NULL UNIQUE,
    utenteRef INTEGER NOT NULL,
    FOREIGN KEY (utenteRef) REFERENCES utente(utenteId)
    
);
CREATE TABLE oggetto(
 oggettoId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
 nomeOggetto VARCHAR(20) NOT NULL,
 descrizione TEXT
);

CREATE TABLE ordine_oggetto(
 oggettoRefId INTEGER NOT NULL,
    ordineRefId  INTEGER NOT NULL,
    PRIMARY KEY (oggettoRefId,OrdineRefId),
    FOREIGN KEY (oggettoRefId) REFERENCES oggetto(oggettoId) ON DELETE CASCADE,
    FOREIGN KEY (ordineRefId) REFERENCES ordine(ordineId) ON DELETE CASCADE
);

INSERT INTO utente(username,pass,tipologia) values
("giovanni","123","admin"),
("gabriele","123","store"),
("melany","123","simple");

INSERT INTO oggetto(nomeOggetto,descrizione) values
("sedia","bella"),
("ARMADIO","descrizone armadio"),
("televisione","descrizione television");

INSERT INTO ordine(codiceOrdine,utenteRef) values
("ogg123",3),
("sed234",1),
("pent234",2);



INSERT INTO ordine_oggetto(oggettoRefId,ordineRefId) values
(1,2),
(2,3),
(1,3);

select *from ordine;
select * from utente;
SELECT * FROM oggetto;
SELECT * FROM ordine_oggetto;