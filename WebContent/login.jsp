<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<title>Pagina di Login</title>
</head>
<body>
	<div class="container">
		<div class="row mt-5">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">
				<h1>PAGINA DI LOGIN</h1>
				<p>Effettua l'accesso all'e-commerce inserendo nome utente e password</p>
			</div>
			<div class="col-md-3"></div>
		</div>
		<div class="row mt-5">
			<div class="col-3"></div>
			<div class="col-6">
				<form name="form_login" action="login" method="POST">
					<div class="form-group">
						<label for="input_username">Username</label> <input type="text"
							class="form-control" name="input_username" />
					</div>
					<div class="form-group">
						<label for="input_password">Password</label> <input
							type="password" class="form-control" name="input_password" />
					</div>
					<button type="button" class="btn btn-success" name="btn_login">LogIn</button>
				</form>


			</div>
			<div class="col-3"></div>
		</div>
	</div>
	
	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="js/login.js"></script>
</body>
</html>