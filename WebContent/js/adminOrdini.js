

function stampaElenco(arr_ordini) {
	let contenuto = "";

	for (let i = 0; i < arr_ordini.length; i++) {
		contenuto += rigaRisultato(arr_ordini[i]);
	}

	$("#contenuto-elenco").html(contenuto);
}

function rigaRisultato(obj) {
	let stringaData = "";
	stringaData += "" + obj.dataOrdine.date.day.toString() + "-";
	stringaData += "" + obj.dataOrdine.date.month.toString() + "-";
	stringaData += "" + obj.dataOrdine.date.year.toString() + " ";
	stringaData += "" + obj.dataOrdine.time.hour.toString() + ":";
	stringaData += "" + obj.dataOrdine.time.minute.toString() + " ";
	let risultato = '<tr data-identificatore="' + obj.ordineId + '">';
	risultato += '    <td>' + obj.codiceOrdine + '</td>';
	risultato += '    <td>' + obj.utenteRef.username + '</td>';
	risultato += '    <td>' + stringaData + '</td>';
	risultato += '    <td><a data-target="#modaleProdottiOrdinati" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#modaleProdottiOrdinati">Lista prodotti</a></td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaOrdine(this)">E</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaOrdine(this)">M</button></td>';
	risultato += '</tr>';

	return risultato;

}

function aggiornaTabella() {
	$.ajax(
		{
			url: "http://localhost:8080/ProgettoFinale/recuperaordini",
			method: "POST",
			success: function(ris_success) {
				//					console.log("TUTTO OK");

				//let risJson = JSON.parse(ris_success);	//Superfluo con encoding
				stampaElenco(ris_success);
				//					console.log(risJson);
			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}




function stampaElencoModale(arr_ordini_modale) {
	let contenutoModale = "";
	for (let i = 0; i < arr_ordini_modale.length; i++) {
		contenutoModale += rigaRisultatoModale(arr_ordini_modale[i]);
	}
	$("#contenuto-elenco-prodotti-ordinati").html(contenutoModale);
}

function rigaRisultatoModale(obj_modale) {
	let risultato = '<tr data-identificatore="' + obj_modale.oggettoId + '">';
	risultato += '    <td>' + obj_modale.nome + '</td>';
	risultato += '    <td>' + obj_modale.descrizione + '</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaProdottoModale(this)">E</button></td>';
	risultato += '</tr>';

	return risultato;

}

function aggiornaTabellaModale() {
	$.ajax(
		{
			url: "http://localhost:8080/ProgettoFinale/recuperaoggetti",
			method: "POST",
			success: function(ris_success) {
				//					console.log("TUTTO OK");

				//let risJson = JSON.parse(ris_success);	//Superfluo con encoding
				stampaElencoModale(ris_success);
				//					console.log(risJson);
			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}


$(document).ready(
	function() {

		aggiornaTabella();			//richiamo l'aggiornamento della tabella al caricamento della pagina

		window.setInterval(
			function() {
				aggiornaTabella();
				console.log("AGGIORNATO");
			}
			, 2000);

		//Qua ci ho piazzato una funzione che mi aggiorni la modale all'apertura
		$('#modaleProdottiOrdinati').on('show.bs.modal', function() {
			aggiornaTabellaModale();
		});
		





	}

);