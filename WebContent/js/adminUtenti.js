

function stampaElenco(arr_utenti) {
	let contenuto = "";

	for (let i = 0; i < arr_utenti.length; i++) {
		contenuto += rigaRisultato(arr_utenti[i]);
	}

	$("#contenuto-elenco").html(contenuto);
}

function rigaRisultato(obj) {

	let risultato = '<tr data-identificatore="' + obj.UtenteId + '">';
	risultato += '    <td>' + obj.username + '</td>';
	risultato += '    <td>' + obj.pass + '</td>';
	risultato += '    <td>' + obj.tipologia + '</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaUtente(this)">E</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaUtente(this)">M</button></td>';
	risultato += '</tr>';

	return risultato;

}

function aggiornaTabella() {
	$.ajax(
		{
			url: "http://localhost:8080/ProgettoFinale/recuperautenti",
			method: "POST",
			success: function(ris_success) {
				//					console.log("TUTTO OK");

				//let risJson = JSON.parse(ris_success);	//Superfluo con encoding
				stampaElenco(ris_success);
				//					console.log(risJson);
			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}

function modificaUtente(objButton) {
	let idSelezionato = $(objButton).parent().parent().data("identificatore");

	$.ajax(
		{
			url: "http://localhost:8080/ProgettoFinale/recuperautente",
			method: "POST",
			data: {
				UtenteId: idSelezionato
			},
			success: function(responso) {			//Senza JSON Parse perché la response è in application/json

				$("#username_edit").val(responso.username);
				$("#password_edit").val(responso.pass);
				$("#ruolo_edit").val(responso.tipologia);
				$("#modaleModifica").data("identificatore", idSelezionato);

				$("#modaleModifica").modal("show");


			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);
}

function eliminaUtente(objButton) {
	let idSelezionato = $(objButton).parent().parent().data("identificatore");

	$.ajax(
		{
			url: "http://localhost:8080/ProgettoFinale/eliminautente",
			method: "POST",
			data: {
				UtenteId: idSelezionato
			},
			success: function(responso) {			//Senza JSON Parse perché la response è in application/json
				switch (responso.risultato) {
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
				}

			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);



	//	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	//	console.log(idSelezionato);
}


$(document).ready(
	function() {

		aggiornaTabella();			//richiamo l'aggiornamento della tabella al caricamento della pagina

		window.setInterval(
			function() {
				aggiornaTabella();
				console.log("AGGIORNATO");
			}
			, 2000);





		$("#btn_inserisci_utente").click(
			function() {
				$("#modaleInserimento").modal("show");
			}
		);


		$("#cta-inserisci").click(
			function() {
				let varUsername = $("#input_username").val();
				let varPassword = $("#input_password").val();
				let varTipologia = $("#input_tipologia").val();

				$.ajax(
					{
						url: "http://localhost:8080/ProgettoFinale/inserisciutente",
						method: "POST",
						data: {
							input_username: varUsername,
							input_password: varPassword,
							input_tipologia: varTipologia
						},
						success: function(risultato) {
							console.log("SONO IN SUCCESS");
							let risJson = JSON.parse(risultato);

							switch (risJson.risultato) {
								case "OK":
									aggiornaTabella();
									$("#modaleInserimento").modal("toggle");
									alert("Inserito con successo");
									break;
								case "ERRORE":
									alert("ERRORE DI INSERIMENTO :(\n" + risJson.dettaglio);
									break;
							}

							console.log(risJson);
						},
						error: function(errore) {
							console.log("SONO IN ERROR");
							let errJson = JSON.parse(errore);
							console.log(errJson);
						}

					}
				);

			}
		);


		$("#cta-modifica").click(
			function() {
				let idSelezionato = $("#modaleModifica").data("identificatore");

				$.ajax(
					{
						url: "http://localhost:8080/ProgettoFinale/modificautente",
						method: "POST",
						data: {
							UtenteId: idSelezionato,
							username: $("#username_edit").val(),
							password: $("#password_edit").val(),
							ruolo: $("#ruolo_edit").val()
						},
						success: function(responso) {
							switch (responso.risultato) {
								case "OK":
									$("#modaleModifica").modal("toggle");
									aggiornaTabella();
									break;
								case "ERRORE":
									alert(responso.risultato + "\n" + responso.dettaglio);
									break;
							}
						},
						error: function(errore) {
							console.log(errore);
						}
					}
				);

			}
		);





	}

);