

function stampaElenco(arr_oggetti){
	let contenuto = "";
	
	for(let i=0; i<arr_oggetti.length; i++){
		contenuto += rigaRisultato(arr_oggetti[i]);
	}
	
	$("#contenuto-elenco").html(contenuto);
}

function rigaRisultato(obj){
	
	let risultato = '<tr data-identificatore="' + obj.oggettoId + '">';
	risultato += '    <td>' + obj.nome + '</td>';
	risultato += '    <td>' + obj.descrizione + '</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaProdotto(this)">E</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaProdotto(this)">M</button></td>';
	risultato += '</tr>';
	
	return risultato;
	
}

function aggiornaTabella(){
	$.ajax(
			{
				url: "http://localhost:8080/ProgettoFinale/recuperaoggetti",
				method: "POST",
				success: function(ris_success){
//					console.log("TUTTO OK");
					
					//let risJson = JSON.parse(ris_success);	//Superfluo con encoding
					stampaElenco(ris_success);
//					console.log(risJson);
				},
				error: function(ris_error){
					console.log("ERRORE");
					console.log(ris_error);
				}
			}
	);
}

function modificaProdotto(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/ProgettoFinale/recuperaoggetto",
				method: "POST",
				data: {
					oggettoId: idSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					
					$("#nome_edit").val(responso.nome);
					$("#descrizione_edit").val(responso.descrizione);
					$("#modaleModifica").data("identificatore", idSelezionato);

					$("#modaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}

function eliminaProdotto(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/ProgettoFinale/eliminaoggetto",
				method: "POST",
				data: {
					oggettoId: idSelezionato
				},
				success: function(responso){			//Senza JSON Parse perché la response è in application/json
					switch(responso.risultato){
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
					}
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
	
	
	
//	let idSelezionato = $(objButton).parent().parent().data("identificatore");
//	console.log(idSelezionato);
}


$(document).ready(
		function(){

			aggiornaTabella();			//richiamo l'aggiornamento della tabella al caricamento della pagina
			
			window.setInterval(
					function(){
						aggiornaTabella();
						console.log("AGGIORNATO");
					}
			, 2000);
			
			
		
		
		
		$("#btn_inserisci_oggetto").click(
					function(){
						$("#modaleInserimento").modal("show");
					}
			);
			
			
			$("#cta-inserisci").click(
					function(){
						let varNome = $("#input_nome").val();
						let varDescrizione = $("#input_descrizione").val();

						$.ajax(
								{
									url: "http://localhost:8080/ProgettoFinale/inseriscioggetto",
									method: "POST",
									data: {
										input_nome: varNome,
										input_descrizione: varDescrizione
									},
									success: function(risultato){
										console.log("SONO IN SUCCESS");
										let risJson = JSON.parse(risultato);
										
										switch(risJson.risultato){
										case "OK":
											aggiornaTabella();
											$("#modaleInserimento").modal("toggle");
											alert("Inserito con successo");
											break;
										case "ERRORE":
											alert("ERRORE DI INSERIMENTO :(\n" + risJson.dettaglio);
											break;
										}
										
										console.log(risJson);
									},
									error: function(errore){
										console.log("SONO IN ERROR");
										let errJson = JSON.parse(errore);
										console.log(errJson);
									}
									
								}
						);
						
					}
			);
			

			$("#cta-modifica").click(
					function(){
						let idSelezionato = $("#modaleModifica").data("identificatore");

						$.ajax(
								{
									url: "http://localhost:8080/ProgettoFinale/modificaprodotto",
									method: "POST",
									data: {
										oggettoId: idSelezionato,
										nome: $("#nome_edit").val(),
										descrizione: $("#descrizione_edit").val()
									},
									success: function(responso){
										switch(responso.risultato){
										case "OK":
											$("#modaleModifica").modal("toggle");
											aggiornaTabella();
											break;
										case "ERRORE":
											alert(responso.risultato + "\n" + responso.dettaglio);
											break;
										}
									},
									error: function(errore){
										console.log(errore);
									}
								}
						);
						
					}
			);	
			
			
			
			
			
		}
		
);