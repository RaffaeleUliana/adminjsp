$(document).ready(
	function() {

		$("button[name=btn_login]").click(
			function() {

				let usr = $("input[name=input_username]").val();
				let pwd = $("input[name=input_password]").val();

				if (usr == "") {
					alert("Errore, il campo \"username\" non può essere vuoto!");
					return;
				}

				if (pwd == "") {
					alert("Errore, il campo \"password\" non può essere vuoto!");
					return;
				}

				$("form[name=form_login]").submit();
			}
		);

	}
);