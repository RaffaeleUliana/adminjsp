package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.progettofinale.classi.ResponsoOperazione;
import com.progettofinale.classi.Utente;
import com.progettofinale.dao.UtenteDAO;


/**
 * Servlet implementation class InserisciUtente
 */
@WebServlet("/inserisciutente")
public class InserisciUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession sessione = request.getSession();
		String tipologia = (String) sessione.getAttribute("tipologia");
		
		if(tipologia !=null && tipologia.equals("admin")) {
			String username = request.getParameter("input_username") != null ? request.getParameter("input_username") : "";
			String password = request.getParameter("input_password") != null ? request.getParameter("input_password") : "";
			String ruolo = request.getParameter("input_tipologia") != null ? request.getParameter("input_tipologia") : "";

			UtenteDAO utDao= new UtenteDAO();
			Utente ut = new Utente();
			ut.setUsername(username);
			ut.setPass(password);
			ut.setTipologia(ruolo);
			
			String strErrore = "";
			
			try {
				utDao.insert(ut);
				
			}catch(SQLException e) {
				e.printStackTrace();
				strErrore = e.getMessage();
			}
			
			PrintWriter out = response.getWriter();
			Gson jsonizzatore = new Gson();
			
			if(ut.getUtenteId() != null) {
				ResponsoOperazione res = new ResponsoOperazione("OK", "");
				out.print(jsonizzatore.toJson(res));
			}
			else {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
				out.print(jsonizzatore.toJson(res));
			}
		}else {
			response.sendRedirect("errore.jsp?tipo_errore=NOT_ALLOWED");
		}
		
		
	}

}
