package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.classi.ResponsoOperazione;
import com.progettofinale.classi.Utente;
import com.progettofinale.dao.UtenteDAO;


/**
 * Servlet implementation class RecuperaUtente
 */
@WebServlet("/recuperautente")
public class RecuperaUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		Integer identificatore = Integer.parseInt(request.getParameter("UtenteId")) ;
		
		
		try {
			
			UtenteDAO utDao = new UtenteDAO();
			
			Utente temp = utDao.getById(identificatore);
			out.print(new Gson().toJson(temp));

		} catch (SQLException e) {

			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));	//Molto compatta
			System.out.println(e.getMessage());
			
		}
	}

}
