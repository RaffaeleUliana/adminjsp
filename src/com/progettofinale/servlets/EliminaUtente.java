package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.classi.ResponsoOperazione;
import com.progettofinale.classi.Utente;
import com.progettofinale.dao.UtenteDAO;

/**
 * Servlet implementation class EliminaUtente
 */
@WebServlet("/eliminautente")
public class EliminaUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");		//In modo da non dover invocare il JSON.parse dal AJAX
		response.setCharacterEncoding("UTF-8");
		Gson jsonizzatore = new Gson();
		
		Integer identificatore = request.getParameter("UtenteId") != null ? Integer.parseInt(request.getParameter("UtenteId")) : -1;
		
		if(identificatore != -1) {

			UtenteDAO utDao = new UtenteDAO();
			try {
				
				Utente ut = utDao.getById(identificatore);
				if(utDao.delete(ut)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "");
					out.print(jsonizzatore.toJson(res));
				}
				else {
					ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a fare l'eliminazione");
					out.print(jsonizzatore.toJson(res));
				}
				
			} catch (SQLException e) {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
				out.print(jsonizzatore.toJson(res));
			}
			
		}
		else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Id non selezionato!");
			out.print(jsonizzatore.toJson(res));
		}
	}

}
