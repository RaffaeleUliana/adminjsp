package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.classi.Oggetto;
import com.progettofinale.classi.ResponsoOperazione;
import com.progettofinale.dao.OggettoDAO;

/**
 * Servlet implementation class ModificaOggetto
 */
@WebServlet("/modificaprodotto")
public class ModificaOggetto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer idProdotto = request.getParameter("oggettoId") != null ? Integer.parseInt(request.getParameter("oggettoId")) : -1;
		String nomeProdotto = request.getParameter("nome") != null ? request.getParameter("nome") : "";
		String descrizioneProdotto = request.getParameter("descrizione") != null ? request.getParameter("descrizione") : "";
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		OggettoDAO oggDao = new OggettoDAO();
		
		if(idProdotto == -1 || nomeProdotto.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "ID o NOME non inseriti!")));
			return;
		}
		
		try {
			Oggetto temp = oggDao.getById(idProdotto);
			
			temp.setNome(nomeProdotto);
			temp.setDescrizione(descrizioneProdotto);
			Oggetto aggiornato = oggDao.update(temp);
			
			if(aggiornato!=null) {
				out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));
				return;
			}
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Modifica non effettuata!")));

			
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Prodotto non trovato!")));
		}
	}

}
