package com.progettofinale.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.progettofinale.classi.Utente;
import com.progettofinale.dao.UtenteDAO;


/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("input_username") != null ? request.getParameter("input_username") : "";
		String pass = request.getParameter("input_password") != null ? request.getParameter("input_password") : "";
		
		UtenteDAO utente_dao = new UtenteDAO();
		Utente utente = null;
		
		try {
			utente = utente_dao.getUtenteByUsername(user);
			
			if(utente != null && pass.equals(utente.getPass())) {
				String tipologia = utente.getTipologia();
				HttpSession sessione = request.getSession();
				sessione.setAttribute("tipologia", tipologia);
				sessione.setAttribute("username", utente.getUsername());
				
				if(tipologia.equalsIgnoreCase("admin")) {
					response.sendRedirect("admin.jsp");
				}else if(tipologia.equalsIgnoreCase("store")){
					response.sendRedirect("store.html");
				}else if(tipologia.equalsIgnoreCase("simple")){
					response.sendRedirect("simple.jsp");
				}else {
					response.sendRedirect("errore.jsp?tipo_errore=RUOLO_ERRATO");
				}
			}else {
				response.sendRedirect("errore.jsp?tipo_errore=LOGIN_ERROR");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			response.sendRedirect("errore.jsp?tipo_errore=SQL_ERROR");
		}
	}

}
