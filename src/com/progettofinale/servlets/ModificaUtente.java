package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.classi.ResponsoOperazione;
import com.progettofinale.classi.Utente;
import com.progettofinale.dao.UtenteDAO;

/**
 * Servlet implementation class ModificaUtente
 */
@WebServlet("/modificautente")
public class ModificaUtente extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer UtenteId = request.getParameter("UtenteId") != null ? Integer.parseInt(request.getParameter("UtenteId")) : -1;
		String username = request.getParameter("username") != null ? request.getParameter("username") : "";
		String password = request.getParameter("password") != null ? request.getParameter("password") : "";
		String ruolo = request.getParameter("ruolo") != null ? request.getParameter("ruolo") : "";

		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		UtenteDAO utDao = new UtenteDAO();
		
		if(UtenteId == -1 || username.trim().equals("") || password.trim().equals("") || ruolo.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "ID o USERNAME o PASSWORD non inseriti!")));
			return;
		}
		
		try {
			Utente temp = utDao.getById(UtenteId);
			
			temp.setUsername(username);
			temp.setPass(password);
			temp.setTipologia(ruolo);;
			Utente aggiornato = utDao.update(temp);
			
			if(aggiornato!=null) {
				out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));
				return;
			}
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Modifica non effettuata!")));

			
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Prodotto non trovato!")));
		}
	}

}
