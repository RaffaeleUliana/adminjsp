package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.progettofinale.classi.Oggetto;
import com.progettofinale.classi.ResponsoOperazione;
import com.progettofinale.dao.OggettoDAO;


/**
 * Servlet implementation class InserisciOggetto
 */
@WebServlet("/inseriscioggetto")
public class InserisciOggetto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sessione = request.getSession();
		String tipologia = (String) sessione.getAttribute("tipologia");
		
		if(tipologia !=null && tipologia.equals("admin")) {
			String nome = request.getParameter("input_nome") != null ? request.getParameter("input_nome") : "";
			String descrizione = request.getParameter("input_descrizione") != null ? request.getParameter("input_descrizione") : "";
			
			OggettoDAO ogg_dao = new OggettoDAO();
			Oggetto oggetto = new Oggetto();
			oggetto.setNome(nome);
			oggetto.setDescrizione(descrizione);
			
			String strErrore = "";
			
			try {
				ogg_dao.insert(oggetto);
				
			}catch(SQLException e) {
				e.printStackTrace();
				strErrore = e.getMessage();
			}
			
			PrintWriter out = response.getWriter();
			Gson jsonizzatore = new Gson();
			
			if(oggetto.getOggettoId() != null) {
				ResponsoOperazione res = new ResponsoOperazione("OK", "");
				out.print(jsonizzatore.toJson(res));
			}
			else {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
				out.print(jsonizzatore.toJson(res));
			}
		}else {
			response.sendRedirect("errore.jsp?tipo_errore=NOT_ALLOWED");
		}
		
	}

}
