package com.progettofinale.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.progettofinale.classi.Utente;
import com.progettofinale.dao.UtenteDAO;


/**
 * Servlet implementation class RecuperaUtenti
 */
@WebServlet("/recuperautenti")
public class RecuperaUtenti extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.html?tipo_errore=NO_GET");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sessione = request.getSession();
		String tipologia = (String) sessione.getAttribute("tipologia")!= null? (String) sessione.getAttribute("tipologia"): null;
		
		if(tipologia != null && tipologia.equals("admin")) {
			UtenteDAO utDao = new UtenteDAO();
			
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			
			try {
				ArrayList<Utente> elenco = utDao.getAll();
				
				String risultatoJson = new Gson().toJson(elenco);
				out.print(risultatoJson);
				
			}catch(SQLException e) {
				e.printStackTrace();
				response.sendRedirect("errore.jsp?tipo_errore=SQL_ERROR");
			}
			
		}else {
			response.sendRedirect("errore.jsp?tipo_errore=NOT_ALLOWED");
		}
	}

}
