package com.progettofinale.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettofinale.classi.Oggetto;
import com.progettofinale.connessioni.ConnettoreDB;

public class OggettoDAO implements Dao<Oggetto>{

	@Override
	public Oggetto getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	oggettoId, nomeOggetto, descrizione FROM oggetto WHERE oggettoId=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Oggetto temp = new Oggetto();
		temp.setOggettoId(risultato.getInt(1));
		temp.setNome(risultato.getString(2));
		temp.setDescrizione(risultato.getString(3));

		return temp;
	}

	@Override
	public ArrayList<Oggetto> getAll() throws SQLException {
		ArrayList<Oggetto> elenco = new ArrayList<Oggetto>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	oggettoId, nomeOggetto, descrizione FROM oggetto WHERE 1=1";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Oggetto temp = new Oggetto();
			temp.setOggettoId(risultato.getInt(1));
			temp.setNome(risultato.getString(2));
			temp.setDescrizione(risultato.getString(3));
			elenco.add(temp);
		}

		return elenco;
	}

	@Override
	public void insert(Oggetto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO oggetto (nomeOggetto, descrizione) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getDescrizione());
		

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setOggettoId(risultato.getInt(1));
		
	}

	@Override
	public boolean delete(Oggetto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM oggetto WHERE oggettoId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getOggettoId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	@Override
	public Oggetto update(Oggetto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE oggetto SET nomeOggetto = ?, descrizione = ? WHERE oggettoId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getNome());
		ps.setString(2, t.getDescrizione());
		ps.setInt(3, t.getOggettoId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getOggettoId());

		return null;
	}

}
