package com.progettofinale.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.progettofinale.classi.Oggetto;
import com.progettofinale.classi.Ordine;
import com.progettofinale.connessioni.ConnettoreDB;

public class OrdineDAO implements Dao<Ordine> {

	@Override
	public Ordine getById(Integer id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	ordineId, dataOrdine, codiceOrdine, utenteRef FROM ordine WHERE ordineId=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		UtenteDAO utDao = new UtenteDAO();

		Ordine temp = new Ordine();
		temp.setOrdineId(risultato.getInt(1));
		temp.setDataOrdine(risultato.getTimestamp(2).toLocalDateTime());
		temp.setCodiceOrdine(risultato.getString(3));
		temp.setUtenteRef(utDao.getById(risultato.getInt(4)));
		
		ArrayList<Oggetto> listaOggetti = new ArrayList<Oggetto>();
		listaOggetti = this.getListaOggetti(temp.getOrdineId());
		
		temp.setListaOggetti(listaOggetti);

		return temp;
	}

	@Override
	public ArrayList<Ordine> getAll() throws SQLException {
		ArrayList<Ordine> elenco = new ArrayList<Ordine>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT 	ordineId, dataOrdine, codiceOrdine, utenteRef FROM ordine WHERE 1=1";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();

		UtenteDAO utDao = new UtenteDAO();

		while (risultato.next()) {

			Ordine temp = new Ordine();
			temp.setOrdineId(risultato.getInt(1));
			temp.setDataOrdine(risultato.getTimestamp(2).toLocalDateTime());
			temp.setCodiceOrdine(risultato.getString(3));
			temp.setUtenteRef(utDao.getById(risultato.getInt(4)));
			
			ArrayList<Oggetto> listaOggetti = new ArrayList<Oggetto>();
			listaOggetti = this.getListaOggetti(temp.getOrdineId());
			
			temp.setListaOggetti(listaOggetti);
			
			elenco.add(temp);
		}

		return elenco;
	}

	@Override
	public void insert(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO ordine (codiceOrdine, utenteRef) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getCodiceOrdine());
		ps.setInt(2, t.getUtenteRef().getUtenteId());

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setOrdineId(risultato.getInt(1));

		for (int i = 0; i < t.getListaOggetti().size(); i++) {
			Oggetto obj = t.getListaOggetti().get(i);
			insertTabellaAppoggio(obj.getOggettoId(), t.getOrdineId());
		}

	}

	@Override
	public boolean delete(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM ordine WHERE ordineId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getOrdineId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	@Override
	public Ordine update(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE ordine SET dataOrdine = ?, codiceOrdine = ?, utenteRef = ? WHERE ordineId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setTimestamp(1, Timestamp.valueOf(t.getDataOrdine()));
		ps.setString(2, t.getCodiceOrdine());
		ps.setInt(3, t.getUtenteRef().getUtenteId());
		ps.setInt(4, t.getOrdineId());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return getById(t.getOrdineId());

		return null;
	}

	private void insertTabellaAppoggio(Integer idOggetto, Integer idOrdine) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "INSERT INTO ordine_oggetto (oggettoRefId, ordineRefId) VALUES (?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, idOggetto);
		ps.setInt(2, idOrdine);

		ps.executeUpdate();
	}

	private ArrayList<Oggetto> getListaOggetti(Integer idOrdine) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "SELECT oggettoRefId FROM ordine_oggetto WHERE ordineRefId=?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, idOrdine);
		
		
		ArrayList<Oggetto> lista = new ArrayList<Oggetto>();
		
		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {
			Integer idOggetto = risultato.getInt(1);
			OggettoDAO oggDao = new OggettoDAO();
			Oggetto temp = oggDao.getById(idOggetto);
			lista.add(temp);
		}

		return lista;
	}

}
