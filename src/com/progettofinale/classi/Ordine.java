package com.progettofinale.classi;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ordine {
	
	private int ordineId;
	private LocalDateTime dataOrdine;
	private String codiceOrdine;
	private Utente utenteRef;
	private ArrayList<Oggetto> listaOggetti = new ArrayList<Oggetto>();
	
	public Ordine() {
		
	}
	public Ordine(LocalDateTime dataOrdine, String codiceOrdine, Utente utenteRef) {
	
		this.dataOrdine = dataOrdine;
		this.codiceOrdine = codiceOrdine;
		this.utenteRef = utenteRef;
	}
	public int getOrdineId() {
		return ordineId;
	}
	public void setOrdineId(int ordineId) {
		this.ordineId = ordineId;
	}
	public LocalDateTime getDataOrdine() {
		return dataOrdine;
	}
	public void setDataOrdine(LocalDateTime dataOrdine) {
		this.dataOrdine = dataOrdine;
	}
	public String getCodiceOrdine() {
		return codiceOrdine;
	}
	public void setCodiceOrdine(String codiceOrdine) {
		this.codiceOrdine = codiceOrdine;
	}
	public Utente getUtenteRef() {
		return utenteRef;
	}
	public void setUtenteRef(Utente utenteRef) {
		this.utenteRef = utenteRef;
	}
	public ArrayList<Oggetto> getListaOggetti() {
		return listaOggetti;
	}
	public void setListaOggetti(ArrayList<Oggetto> listaOggetti) {
		this.listaOggetti = listaOggetti;
	}
	@Override
	public String toString() {
		return "Ordine [ordineId=" + ordineId + ", dataOrdine=" + dataOrdine + ", codiceOrdine=" + codiceOrdine
				+ ", utenteRef=" + utenteRef + "]";
	}
	
	
	
	
	

}


