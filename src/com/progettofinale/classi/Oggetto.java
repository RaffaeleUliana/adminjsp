package com.progettofinale.classi;

public class Oggetto {
	private Integer oggettoId;
	private String nome;
	private String descrizione;
	
	public Oggetto() {
		
	}

	public Integer getOggettoId() {
		return oggettoId;
	}

	public void setOggettoId(Integer oggettoId) {
		this.oggettoId = oggettoId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Oggetto [oggettoId=" + oggettoId + ", nome=" + nome + ", descrizione=" + descrizione + "]";
	}
	
}
